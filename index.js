// console.log("Hello Zawarudo!");


// [Section] Exponent operator

	// before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);

	// ES6 update
		const secondNum = Math.pow(8, 2);
		console.log(secondNum);

// [Section] Template Literals
	/*
		- allows us to write strings w/o using concat operator(++)
		- helps with code readability
	*/

	let name = "John";
	// before template literals we use 'SINGLE QUOTE' or "DOUBLE QUOTE"
		let message = 'Hello ' + name + '! Welcome to Programming!'; 
		console.log(message);
	// using template literals uses backticks (``);
		message = `Hello ${name}! 
		Welcome to 
				WebDev!`;
		console.log(message);

		// template literals allows us to write strings w/ embedded JavaScript

		const interestRate = 0.1;
		const principal = 1000;

		console.log(`The interst on your savings account is: ${interestRate * principal}`)

// [Section] Array Desctructioring
	// allows us to unpack elements in arrays into distinct variables
	// allows us to name array elements w/ variables instead of using numbers and helps us w/ code readability
	/*
		Syntax:
			let/const [varName, varNameB, ...] = arrayName;
	*/

		const fullName = ["Juan", "Dela", "Cruz"];

		// before array destructioring - "TRADITIONAL"
			console.log(fullName[0]); // [1],[2],[3] so on and so for
			console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! Nice to meet you.`);

		// Array destructioring
			const [firsName, middleName, lastName] = fullName;
			console.log(firsName); // so on and so for
			// console.log(middleName); console.log(lastName); 
			// console.log(fullName);

// [Section] Object Destructuring

	// allows us to unpack properties of pbjects into distinct variables
	/*
		syntax:
			let/const { propertyNameA, propertyNameB, ...} = objectName;
	*/

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Carcel"
		}

		// before object destructuring  - "TRADITIONAL"
			console.log(person.givenName);
			console.log(person['maidenName']);
			console.log(person.familyName);

		// Object destructuring
			let {givenName, maidenName, familyName} = person;
			console.log(givenName); // so on and so for
			// console.log(maidenName); console.log(familyName);

		function getFullName({givenName, maidenName, familyName}){
			console.log(`${givenName} ${maidenName} ${familyName}.`)
		}

		getFullName(person);

// [Section] Arrow Functions
	// compact alternative syntax to traditional functions.
	// useful for code snippets where creating functions will not be reused in any other portion of the code.

	// Arrow Function =>

		const hello = () => {
			console.log(`Hello World!`);
		}
		hello();

		// before arrow function and template literals - "TRADITIONAL"
			function printFullName(firstName, middleInitial, lastName)
			{
				console.log(firsName + ' ' + middleInitial + ' ' + lastName);
			}

			printFullName("Chris", 'O.', "Quesada");

		// after arrow function and template literals
			let fName = (firsName, middleInitial, lastName) =>
			{
				console.log(`${firsName} ${middleInitial} ${lastName}`)
			}

			fName("Rudeus", 'N.', "Greyrat");

	// Arrow functions with Loops

		const student = ["John" , "Jane", "Judith"];
		// before arrow function - "TRADITIONAL"
			function iterate(student){
				console.log(student + " is a student.");
			}
			student.forEach(iterate)

		// after arrow function
			student.forEach((student) => console.log(`${student} is a Hero`));


// [Section] Implicit Return Statement
	/*
		There are instances when you can omit return statements, this works because even w/o return statements JS implicitly adds it for the result of the function.
	*/

	const add = (x,y) => {
		// console.log(x + y);
		return x + y;
	}

	let sum = add(23, 45);
	console.log(`Sum contained in add Variable: ${sum}`);

	const sub = (x,y) => x-y;

	let difference = sub(10, 6);
	console.log(`Difference contained in sub Variable: ${difference}`)


// [Section] Default Function Argument Value

	const greet = (name = 'User') => { // if there are no inputs, we can have a default value
		return `Good morning, ${name}!`;
	}

	console.log(greet()) // if no inputs, you should put default value on the function name


// [Section] Class-based Object Blueprints
	// allows is to create/instantiation of objects using classes blueprints

	// in creating a class a constructor is a special method of a class for creating/initializing an object for that class.

	/*
		syntax:
			class ClassName{
				constructor(objectValueA, objectValueB,...){
					this.objectPropertyA = objectValueA;
					this.objectPropertyB = objectValueB;
				}
			}
	*/

		class Car{
			constructor(brand, name, year){
				this.carBrand = brand;
				this.carName = name;
				this.carYear = year;
			}
		}

		let car = new Car("Toyota", "Hilux-pickup" , 2019);
		console.log(car);